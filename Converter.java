public class Converter {

    /*
     *@Authors: Nadim Asaduzzaman
     *          Xiang Zhang
     *          Albin Radaj
     * 
     */

    private double celciusToFarenheit(double C){
    	return (C*9/5)+32;
    }
    private double farenheitToCelcius(double F){
        return (F-32)*5/9;
    }
    public static void main(String[] args)
    {
        Converter ctr = new Converter();
        System.out.println("180 Celcius to Fahrenheit: " + String.valueOf(ctr.celsiusToFahrenheit(180)));
        System.out.println("250 Fahrenheit to Celcius: " + String.valueOf(ctr.farenheitToCelcius(250)));
    }
}
